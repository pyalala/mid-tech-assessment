# Code Review 

- Line 5
> Correct spelling for `feul`

- Line 8-15 & 17-21
> Merge both useEffect's into one and remove the fuel dependency as having two useEffect's in this case is redundant

- Line 9
> Add error handling for ajax calls

- Line 9
> rewrite useEffect to use `async`/`await`

- Line 13
> Remove `console.log`

- Line 18
> Remove `else` and replace with `return`

- Line 19
> Remove `else` and replace with `return`

- Line 25
> Replace string matching condition and check for `fuel.litres === 0`

- Line 25
> Change the ternary operator to `style={{ color: fuel.liters === 0 ? "red" : "" }}`

- Line 25
> If possible use class name instead of an inline style