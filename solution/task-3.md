### Front End

a. Tools, frameworks, npm libraries

- As instructed, I would be using React due to limited skill set of the team
- As for tools, I would recommend using standard open source tool as recommended by Facebook, the CRA which provides out of the box support to ES6, TS, Coverage Reports, and comes bundled with custom bundler to bundle js, css, and image assets, a built in dev server and Unit testing tool
- Additionally I would be using npm libraries such as React Helmet for custom titles/meta information, React Router to handle the routing of the pages, Probably React Cookies (for isomorphism)

b. Directory Structure

- I tend to keep the base directory structure of the project created using a CRA tool. The minor changes I'd make would be adding folders for Hooks, Components, Providers, Helpers, Services, etc to keep these items well organized.

c. List of components

- The following are the components that I would use to build the app:
 
  -  Home - The base page
  -  Layout - Layout page which contains routing, providers, and helmet content
  -  Profile Listing Page - profile listing component container which would contain a list of Profile Listing Components
  -  Profile Listing Component - component which would render the image, name and email
  -  Profile Page - page which would render the profile
  -  Profile Page Listing Component - If the content of profile is different than the component mentioned above, this component would include the above component as well as additional information
  -  Additional Components - Any additional components as required

d.  Component Directory Structure

```
  Home 
   └ Profile Listing Page 
      └ Profile Listing
        └ Profile
           └ Image
           └ Profile Details
```
e. Traditional vs SPA

* Benefits of SPA 
  
  - It is fast as basic resources are loaded first and actual content is loaded incrementally.
  - No need to render whole page
  - Easy to debug using browser 
  - Data can be cached thus storing entire app on client's browser making the site blazing fast once cached


* Drawbacks of SPA 
  
  - It is slow as whole content needs to be loaded at once
  - Very hard to make it SEO optimized
  - Depending on framework chosen, it sometimes takes time to load the whole framework in memory, thus slowing down the performance
  - Needs JS to be Enabled
  - It is not secure as it's prone to client side attacks (XSS, network sniffing etc.) and possible memory leaks


* Benefits of Traditional
  
  - It is very good for SEO as content is rendered as whole
  -  Offers good security in terms of handling malformed requests/XSS, etc.
  - Very Scalable 


* Drawbacks of Traditional 
  
  - Frontend (html,js,css) and back end are tightly integrated
  - Longer development times compared to SPAs 
  - Performance is slow compared to SPAs


f. Comments/Suggestions

As the team has skills in developing React and Node.JS, I would recommend the team members to up skill themselves in a SSR/SSG framework like Next.JS/Sapper to implement the project   

### Back End

a. Short Description of API

  - GET / - returns all profiles as a JSON array
  - GET /{profile-id} - returns a specific profile whose unique id has been passed as a JSON object 

b. Framework

  - I would personally suggest using Express.JS as it handles bootstrapping the project and inclusion of Content and Body Parsers and it has a good Middleware integration support.
  - I would also use npm libraries for rate limiting and request throttling strategies to make sure that the server does not freeze up under load