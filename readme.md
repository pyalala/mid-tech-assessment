# Instructions

Node.JS is required for running the JavaScript solutions. Although it is mandatory, the code can also be run in Browser by copy pasting the code in the console and following the instructions mentioned in the file below to run them manually.

## JavaScript

Code for tasks 2 and 4 are located in `src/task-2.js` and `src/task-4.js` respectively.

Corresponding Test cases fot the tasks are located in `test/task-2.test.js` and `test/task-4.test.js`

## Technical

Write up for tasks 3 and 5 are located in `solution/task-3.md` and `solution/task-5.md`

These files can be read using any Markdown editor or on Github/Bitbucket page.

## How to run JavaScript 

Before you begin, In order to run test cases, install the necessary testing framework packages by running `yarn install` or `npm install` (depending on your setup)

#### Auto

simply run `yarn test` to start testing the code or follow the instructions below to test the file manually.

### task-2.js

#### Manual 

Add the following code to the JS:

`console.log(Provider.getWeather("London"));`

`console.log(Provider.findCity(51.5074,0.1278));`

`console.log(Provider.getWeather((Provider.findCity(51.5074,0.1278))));`

and run using `node src/task-2.js`

### task-4.js

#### Manual 

Add the following code to the JS:

`console.log(getProcessingPage([{ state: "processing" }, { state: "error" }]))`

`console.log(getProcessingPage([{ state: "processing" }, { state: "error", errorCode: "NO_STOCK" }]))`

and run using `node src/task-4.js`