class Provider {
    static getWeather(city) {
        if (!city) return null;
        return Promise.resolve(`The weather of ${city} is Cloudy`)
    }

    static getLocalCurrency(city) {
        if (!city) return null;
        return Promise.resolve(`The local currency of ${city} is GBP`)
    };

    static findCity(long, lat) {
        if (!long || !lat) return null;
        return Promise.resolve(`London`)
    };
}

module.exports = Provider;