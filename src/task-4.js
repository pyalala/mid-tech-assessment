const ERROR_CODES = {
    'NO_STOCK': { "title": "Error page", "message": "No stock has been found" },
    'INCORRECT_DETAILS': { "title": "Error page", "message": "Incorrect details have been entered" },
    "NULL": { "title": "Error page", "message": null },
    "UNDEFINED": { "title": "Error page", "message": null }
}

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

/** 
 * Gets the processing page
 * @param {array} data  
*/

async function getProcessingPage(data) {
    let response = null;

    if (!data?.length) {
        response = ERROR_CODES["UNDEFINED"]
    } else {
        for (let i = 0; i <= data.length; i++) {
            const state = data[i]?.state;
            const errorCode = data[i]?.errorCode;
            if (state === "processing") {
                await delay(2000);
                await getProcessingPage(data.slice(i + 1));
                return;
            }
            if (state === "success") {
                response = { "title": "Order complete", "message": null };
            }
            if (state === "error") {
                response = typeof errorCode === "undefined" ? ERROR_CODES["UNDEFINED"] : errorCode == null ? ERROR_CODES["NULL"] : ERROR_CODES[errorCode]
            }
        }
    }
    return response;
}

module.exports = getProcessingPage;