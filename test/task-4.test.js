const expect = require('chai').expect;
const getProcessingPage = require("../src/task-4");

const STATES = {
    PROCESSING: { state: "processing" },
    SUCCESS: { state: "success" },
    ERROR: { state: "error" }
}

const ERRORS = {
    'NO_STOCK': 'NO_STOCK',
    'INCORRECT_DETAILS': 'INCORRECT_DETAILS'
};

const RESPONSE = {
    SUCCESS: { title: 'Order complete', message: null },
    ERROR: {
        'NO_STOCK': { title: 'Error page', message: 'No stock has been found' },
        'INCORRECT_DETAILS': { title: 'Error page', message: 'Incorrect details have been entered' },
        'NULL': { title: 'Error page', message: null },
        'UNDEFINED': { title: 'Error page', message: null }
    }
}


describe('Testing Processing Page', function () {

    describe('Processing', function () {
        it(`Should display ${JSON.stringify(RESPONSE.ERROR.NULL)} when only ${JSON.stringify(STATES.PROCESSING)} is passed`, function (done) {
            const processingResult = getProcessingPage([STATES.PROCESSING]);
            processingResult.then(function (resolve, reject) {
                expect(resolve).to.deep.equal(RESPONSE.UNDEFINED);
            });
            done()
        });

    });

    describe('Success', function () {
        it(`Should display ${JSON.stringify(RESPONSE.SUCCESS)} message when ${JSON.stringify(STATES.SUCCESS)} is passed`, function (done) {
            const successResult = getProcessingPage([STATES.PROCESSING, STATES.SUCCESS]);
            successResult.then(function (resolve, reject) {
                expect(resolve).to.deep.equal(RESPONSE.SUCCESS);
            });
            done()
        });
    });

    describe('Error', function () {
        it(`Should display ${JSON.stringify(RESPONSE.ERROR.UNDEFINED)} message when only ${JSON.stringify(STATES.ERROR)} is passed`, function (done) {
            const errorResult = getProcessingPage([STATES.PROCESSING, STATES.ERROR]);
            errorResult.then(function (resolve, reject) {
                expect(resolve).to.deep.equal(RESPONSE.ERROR.UNDEFINED);
            });
            done()
        });

        it(`Should display ${JSON.stringify(RESPONSE.ERROR.NO_STOCK)} message when only ${JSON.stringify(Object.assign(STATES.ERROR, { errorCodes: ERRORS.NO_STOCK }))} is passed`, function (done) {
            const errorResult = getProcessingPage([STATES.PROCESSING, Object.assign(STATES.ERROR, { errorCodes: ERRORS.NO_STOCK })]);
            errorResult.then(function (resolve, reject) {
                expect(resolve).to.deep.equal(RESPONSE.ERROR.NO_STOCK);
            });
            done()
        });

        it(`Should display ${JSON.stringify(RESPONSE.ERROR.INCORRECT_DETAILS)} message when only ${JSON.stringify(Object.assign(STATES.ERROR, { errorCodes: ERRORS.INCORRECT_DETAILS }))} is passed`, function (done) {
            const errorResult = getProcessingPage([STATES.PROCESSING, Object.assign(STATES.ERROR, { errorCodes: ERRORS.INCORRECT_DETAILS })]);
            errorResult.then(function (resolve, reject) {
                expect(resolve).to.deep.equal(RESPONSE.ERROR.INCORRECT_DETAILS);
            });
            done()
        });
    });

    describe('Multiple States', function () {
        it(`Should display ${JSON.stringify(RESPONSE.SUCCESS)} message when [${JSON.stringify(STATES.PROCESSING)}, ${JSON.stringify(STATES.SUCCESS)}] is passed`, function (done) {
            const successResult = getProcessingPage([STATES.PROCESSING, STATES.SUCCESS]);
            successResult.then(function (resolve, reject) {
                expect(resolve).to.deep.equal(RESPONSE.SUCCESS);
            });
            done()
        });

        it(`Should display ${JSON.stringify(RESPONSE.ERROR.NO_STOCK)} message when [${JSON.stringify(STATES.PROCESSING)}, ${JSON.stringify(Object.assign(STATES.ERROR, { errorCodes: ERRORS.NO_STOCK }))}] is passed`, function (done) {
            const successResult = getProcessingPage([STATES.PROCESSING, STATES.SUCCESS]);
            successResult.then(function (resolve, reject) {
                expect(resolve).to.deep.equal(RESPONSE.ERROR.NO_STOCK);
            });
            done()
        });
    });
});