const expect = require('chai').expect;
const Provider = require("../src/task-2");

describe('Provider API', function () {

    const city = "London";
    const lat = 51.5074;
    const lng = 0.1278;

    describe('Weather API', function () {
        it('Should return a string', async function () {
            const londonWeather = await Provider.getWeather(city);
            expect(londonWeather).to.be.not.empty;
        });

        it(`Should return weather for ${city} as Cloudy`, async function () {
            const londonWeather = await Provider.getWeather(city);
            expect(londonWeather).to.have.string(`The weather of ${city} is Cloudy`);
        });

        it('Should return `null` if no city is passed in', async function () {
            const localWeather = await Provider.getWeather();
            expect(localWeather).to.be.null;
        });
    });

    describe('Currency API', function () {
        it('Should return a string', async function () {
            const londonCurrency = await Provider.getLocalCurrency(city);
            expect(londonCurrency).to.be.not.empty;
        });

        it(`Should return currency for ${city} as GBP`, async function () {
            const londonCurrency = await Provider.getLocalCurrency(city);
            expect(londonCurrency).to.have.string(`The local currency of ${city} is GBP`);
        });

        it('Should return `null` if no city is passed in', async function () {
            const localCurrency = await Provider.getLocalCurrency();
            expect(localCurrency).to.be.null;
        });
    });

    describe('City API', function () {
        it('Should return a string', async function () {
            const cityWeather = await Provider.findCity(lat, lng);
            expect(cityWeather).to.be.not.empty;
        });

        it(`Should return ${city} city`, async function () {
            const searchedCity = await Provider.findCity(lat, lng);
            expect(searchedCity).to.have.string(city);
        });

        it('Should return `null` if no city is passed in', async function () {
            const searchedCity = await Provider.findCity();
            expect(searchedCity).to.be.null;
        });
    });



    describe('City and Weather Combination API', function () {
        it('Should return a string', async function () {
            const searchedCity = await Provider.findCity(lat, lng);
            expect(searchedCity).to.be.not.empty;

            const cityWeather = await Provider.getWeather(searchedCity);
            expect(cityWeather).to.be.not.empty;
        });

        it(`Should return ${city} city`, async function () {
            const searchedCity = await Provider.findCity(lat, lng);
            expect(searchedCity).to.have.string(city);
        });

        it(`Should return ${city} Weather as Cloudy`, async function () {
            const searchedCity = await Provider.findCity(lat, lng);
            const cityWeather = await Provider.getWeather(searchedCity);
            expect(cityWeather).to.have.string(`The weather of ${searchedCity} is Cloudy`);
        });

        it('Should return `null` for city if no lat or lng is passed in', async function () {
            const searchedCity = await Provider.findCity();
            expect(searchedCity).to.be.null;
        });

        it('Should return `null` for weather if no city is passed in', async function () {
            const searchedCity = await Provider.findCity();
            const cityWeather = await Provider.getWeather(searchedCity);
            expect(cityWeather).to.be.null;
        });
    });

});
